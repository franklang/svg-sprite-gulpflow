'use strict';

import plugins       from 'gulp-load-plugins';
import gulp          from 'gulp';
import rimraf        from 'rimraf';
import yaml          from 'js-yaml';
import fs            from 'fs';

// Load all Gulp plugins into one variable
const $ = plugins();

// Load settings from settings.yml
const { PATHS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// Build the "dist" folder by running all of the below tasks
gulp.task('build',
 gulp.series(clean, svgSprite));

// Build the site, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', watch));

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
  rimraf(PATHS.dist, done);
}

// Generate SVG sprite
function svgSprite() {
  return gulp.src(PATHS.svgSpriteSource)
    .pipe($.svgmin())
    .pipe($.rename({
      prefix: 'icon-'
    }))
    .pipe($.svgstore({
      inlineSvg: true
    }))
    .pipe($.cheerio({
      run: function ($, file) {
          $('svg').css('display', 'none')
          $('[fill]').removeAttr('fill')
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe($.rename('svg-icon-sprite.phtml'))
    .pipe(gulp.dest(PATHS.svgSpriteDestination));
}

// Watch for changes to SVG sprite source folder
function watch() {
  gulp.watch(PATHS.svgSpriteSource).on('all', gulp.series(svgSprite));
}
