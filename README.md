# SVG sprite Gulpflow

Generate SVG sprites with Gulp.


**Required Node version: "8"** (which is already configured within the provided Docker container, so this information is only useful if you want to use/install Node/NPM locally).

## Setup dev environment with Docker and Docker Compose (recommended)



### Install Docker

In order to install Docker, please refer to: [Docker Installation in Ubuntu 18.04 (works for 20.04)](https://medium.com/@sh.tsang/installation-of-docker-3b18d9e70bea)

Après l'install de Docker CE (donc __après__ avoir fait tout ce qui est décrit dans le lien ci-dessus), lancer en plus la commande pour installer le CLI:
```
sudo apt install docker-ce-cli
```



### Install Docker Compose

Pour docker compose ne pas installer via apt mais suivre les instructions sur leur site (onglet "Linux"): [Install Docker Compose](https://docs.docker.com/compose/install/)

Vérifier les versions des installs (le point bloquant est souvent docker-compose, une version < 1.18 et nos docker compose ne marchent plus):
```
docker --version
docker-compose –version
```

Ajouter son user au groupe docker:
```
sudo usermod -aG docker $USER
```

Après, soit tu redémarres ton ordi/ta VM, soit pour chaque terminal jusqu'au prochain redémarrage tu taperas:
```
newgrp docker
```


### Build and Run "Node" Docker container to process Gulp tasks

From the root folder where `Dockerfile` and `docker-compose.yml` files are stored:
```
// Build node container (only execute once, at project init):
docker build -t node .
```
```
// Then, run the service (access to the container shell):
docker-compose run --rm --service-ports node_dev_env
```
```
// Exit container:
exit
```
Check Usage Guide below to process Gulp tasks (similar commands for Docker or local NPM environments).

Reference documentation for further information (link to english written tutorial provided): [Utiliser Docker pour créer un environnement de développement NodeJS](https://devfrontend.info/dockernodejs-utiliser-docker-pour-creer-un-environnement-de-developpement-nodejs/)



## Setup without Docker

Installer (ou posséder) la version **8** de nodeJS sur sa machine.
Check Usage Guide below to process Gulp tasks (similar commands for Docker or local NPM environments).


## Usage guide

### Process assets
From the container shell OR (if you don't use Docker) from the root folder where `package.json` and `gulpfile.babel.js` files are stored:
```
// Only execute once, at project init, OR whenever a new dependency is added to the package.json file:
npm install
```
```
// Compile front-end assets:
yarn start
```

```
// Remove NPM dependencies before re-executing a fresh 'npm install':
rm -rf node_modules && rm package-lock.json
```



## Troubleshooting

### Erreurs de droits ou d'accès pendant l'opération de build du container Docker
Tapez la commande ci-dessous avant de ré-éxécuter un build:
```
newgrp docker
```

### Erreurs de droits avec NPM
Erreurs de droits en général ou cette erreur à l'exécution de la commande yarn start: "npm WARN saveError EACCES: permission denied, open '/home/app/package-lock.json".
Placez-vous à la racine de votre thème (si vous êtes dans le container Docker Node, sortez-en au préalable!) et tapez:
```
// Remove NPM dependencies before re-executing a fresh 'npm install':
chmod -R 777 assets
```



Feel free to adjust this bash script to your needs, but make sure to keep both `gulpfile.babel.js` and `config.yml` files up-to-date!
